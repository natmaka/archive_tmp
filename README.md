# archive_tmp

This bash script offers a (sort of) COMMIT and ROLLBACK (or way to enjoy a 'snapshot') for a directory.

It creates an archive (tar) of the current directory content, placed in a dedicated directory.  It then apparently finishes and you can process with whatever you want to do while the script compresses (bz2) the archive (thanks to a background process).

Use case: you want to temporarily keep the contents of the current directory, for example because you plan to do something dangerous for it and prefer to be able to restore it (partially or completely) later. 

It follows symlinks (saves the file pointed by any symlink, instead of saving the symlink).

Tested under Linux.

Prerequisites: bash, mktemp, hostname, ionice, nice, tar, pbzip2 (parallel bzip2)

## To install it

In the script read the lines between
`#INSTALLATION BEGIN` and `#INSTALLATION END`

## To use it

### to save a directory's contents
1. in a shell (command line) go to the directory you plan to backup
2. invoke this script

### to restore
1. go to an empty directory
2. untar (tar xjpf) the archive
3. read the file named Archive_tmp_* , it reveals when and where the archive was created, and the contents of the original directory

If you don't have the archive name anymore then explore the archive directory (its name is declared in the script by $ARCHIVEDIR).

Format of an archive name: H__N_D_H_S.tar.bz2
where:
- H: hostname (where the archive was created)
- N: directory name
- D: date
- H: hour
- S: nanoseconds part of the hour (neglect this!)

If pbzip2 overloads your computer then modify the script to invoke it with its '-l' argument.

